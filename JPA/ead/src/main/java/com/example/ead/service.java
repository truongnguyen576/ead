package com.example.ead;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class service {
    private final repository repository;

    public List<user> findAll(){
        return repository.findAll();
    }

    public user save(user user){
        return repository.save(user);
    }
}
