package com.example.ead;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class controller {
    private final service service;
    @GetMapping
    public String index(Model model){
        List<user> users = service.findAll();
        model.addAttribute("users",users);
        return "index";
    }
}
